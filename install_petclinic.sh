# Set variables for SSH
KEYLOC=$1
IPADDRESS=$2


ssh -o StrictHostKeyChecking=no -i $KEYLOC ec2-user@$IPADDRESS << EOF

# Clone the pet clinic repo 
git clone https://github.com/spring-projects/spring-petclinic.git

## Commands needed to run (Do not work)
# cd spring-petclinic
# ./mvnw spring-boot:run

EOF