# Set variables for ssh
KEYLOC=$1
IPADDRESS=$2


ssh -o StrictHostKeyChecking=no -i $KEYLOC ec2-user@$IPADDRESS << EOF

% Install Git features
sudo yum install git-all -y

EOF
