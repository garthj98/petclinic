# Pet Clinic

## Pre Requisites 
- Copy repo using `git clone git@bitbucket.org:garthj98/petclinic.git`
- 1 Ubuntu instance for load balancing 
  - The load balancer should allow http and https access to all and port 22 to work/home IP and jenkisns machine 
- 2 amazon linux 2 machines for pet clinic 
  - Web servers should only allow port 22 to work/home IP and jenkisns machine and 8080 only to the load balancers private IP adress (security groups assess2)
- 1 RDS where:
  - Username is petclinic
  - Password is petclinic
  - Security should allow access from servers in the public net (synamic-devops-db security group)
- 1 Amazon linux for mariaDB
  - (allow Jenkins and cohort-9 IPs) 
- 1 functioning Jenkins server

***IN THE JENKINS petclinic-CD BUILD SHELL ENTER THESE COMANDS AND PUSH OR PRESS BUILD NOW IN THE CD PIPELINE TO BUILD***

### Steps to set up pet clinic 
- Run `./run.sh $1 $2`
  1. Location of the ssh key `/path/to/your/key`
  2. Input variable is for the private IP of 1 of the amazon linux machine (public if not doing in Jenkins)
- You then need to repeat these steps for the second amazon linux server 

### Steps to get the database running 
- Run `./run_database.sh $1 $2 $3 $4 $5`
  1. Location of the ssh key `/path/to/your/key`
  2. Is private IP of the sql server (public if not doing in Jenkins)
  3. Is the domain name of the RDS database
  4. RDS username
  5. RDS password

### Steps to get the Loadbalancer running 
- Run `./loadbalance.sh $1 $2 $3 $4`
  - Input 3 variables, 
  1. Location of the SSH key `/path/to/your/key`
  2. private IP of the loadbalancer (public if not doing in Jenkins)
  3. private IP of 1st pet clinic server 
  4. private IP of 2nd pet clinic server 

***PUSH OR EUN THESE SCRIPTS NOW, BEFORE MANUAL SET UP ***

### Connecting the database to pet clinic and starting the aplication 
- Log into a pet clinic server using `ssh -i /PATH/TO/YOUR/KEY ec2-user@PUBLIC.IP.ADDRESS`
- Edit the application.properties file using `sudo nano spring-petclinic/src/main/resources/application.properties`
- Delete the current lines for database `spring.datasource` to:
  - `spring.datasource.url=jdbc:mysql://localhost/petclinic`
    - Where `localhost` is the localhost of the RDS 
  - `spring.datasource.username=petclinic`
    - Where `petclinic` is the username
  - `spring.datasource.password=petclinic`
    - Where `petclinic` is the password
- Enter the `spring-petclinic` folder using `cd spring-petclinic`
  - Use `mvn -Dmaven.test.skip=true package` to recompile
  - Use `./mvnw spring-boot:run` to boot the servers
- DO THIS FOR BOTH SERVERS RUNNING PET CLINIC
