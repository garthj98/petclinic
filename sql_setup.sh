# Set variables
KEYLOC=$1
IPADDRESS=$2
hostname=$3

# Set user details for created RDS
user=$4 
pass=$5



ssh -o StrictHostKeyChecking=no -i $KEYLOC ec2-user@$IPADDRESS << EOF

# Clone pet clinic 
sudo yum update
sudo yum install git-all -y
git clone https://github.com/spring-projects/spring-petclinic.git

# Set up the SQL database and add in data entries 
mysql -h $hostname -P 3306 -u $user -p$pass < /home/ec2-user/spring-petclinic/src/main/resources/db/mysql/user.sql
mysql -h $hostname -P 3306 -u $user -p$pass petclinic < /home/ec2-user/spring-petclinic/src/main/resources/db/mysql/schema.sql
mysql -h $hostname -P 3306 -u $user -p$pass petclinic < /home/ec2-user/spring-petclinic/src/main/resources/db/mysql/data.sql

EOF