# Set variables
KEYLOC=$1
IPADDRESS=$2
hostname=$3

# Set user details for created RDS
user=$4 
pass=$5

# Run scripts to install and setup
./install_mariaDB.sh $KEYLOC $IPADDRESS
./sql_setup.sh $KEYLOC $IPADDRESS $hostname $user $pass