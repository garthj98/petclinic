# Script to install maven3 (https://www.javahelps.com/2017/10/install-apache-maven-on-linux.html)
# Set variables 
KEYLOC=$1
IPADDRESS=$2


ssh -o StrictHostKeyChecking=no -i $KEYLOC ec2-user@$IPADDRESS << EOF

# Install JDK11 (Java)
sudo yum update -y
sudo amazon-linux-extras install java-openjdk11 -y

# Get files to install maven3
sudo yum update -y
wget https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip

# Unzip maven3 files in opt
## needs to by unzipped in /opt
sudo unzip /home/ec2-user/apache-maven-3.8.4-bin.zip -d /opt/

# Add the path of maven3 to environment (Check version numbers are correct if not working and paths)
sudo sh -c '''
cat >/etc/environment <<_END_

PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-11-openjdk-11.0.13.0.8-1.amzn2.0.3.x86_64/bin/:/opt/apache-maven-3.8.4/bin"
JAVA_HOME="/usr/lib/jvm/java-11-openjdk-11.0.13.0.8-1.amzn2.0.3.x86_64/"
M2_HOME="/opt/apache-maven-3.8.4"

_END_
'''

# Update environment 
sudo update-alternatives --install "/usr/bin/mvn" "mvn" "/opt/apache-maven-3.8.4/bin/mvn" 0
sudo update-alternatives --set mvn /opt/apache-maven-3.8.4/bin/mvn

# Download file to finish install
sudo wget https://raw.github.com/dimaj/maven-bash-completion/master/bash_completion.bash --output-document /etc/bash_completion.d/mvn

EOF
