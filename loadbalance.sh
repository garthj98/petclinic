# Set variables
KEYLOC=$1
hostname=$2
PIP1=$3
PIP2=$4

ssh -o StrictHostKeyChecking=no -i $KEYLOC ubuntu@$hostname << EOF

# Update the ubuntu server 
sudo apt update

# Install haproxy 
sudo apt install haproxy -y



sudo sh -c "cat >> /etc/haproxy/haproxy.cfg <<_END_ 

frontend http_front
    bind *:80
    stats uri /haproxy?stats
    default_backend http_back

backend http_back
    balance roundrobin
    server server1 $PIP1:8080 check
    server server2 $PIP2:8080 check
_END_"

sudo systemctl restart haproxy
EOF

