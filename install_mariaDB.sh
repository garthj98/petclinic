# Set variables 
KEYLOC=$1
IPADDRESS=$2

ssh -o StrictHostKeyChecking=no -i $KEYLOC ec2-user@$IPADDRESS << EOF

# Update system and install mariaDB
sudo yum update -y
sudo yum install mariadb-server -y

# Enable and start mariaDB
sudo systemctl enable mariadb
sudo systemctl start mariadb

# Intall aws packages so it can connect to RDS
sudo yum install amazon-linux-extras -y 
sudo timeout 60 amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2

EOF