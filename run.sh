# Set variable names
KEYLOC=$1
IPADDRESS=$2

# Run scripts to install maven3, git and petclinic 
./install_maven3.sh $KEYLOC $IPADDRESS 
./install_gitlinetool.sh $KEYLOC $IPADDRESS 
./install_petclinic.sh $KEYLOC $IPADDRESS 
